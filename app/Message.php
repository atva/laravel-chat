<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //
    protected $fillable = ['message'];


    //A message is owned by a user.
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
